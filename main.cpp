// lab 2 - Rob Johnson - pcc cs162 - 10/2/18

//Tests
//These are anagrams of each other:
//pit � tip
//skin � kins
//blow � bowl
//alloy � loyal
//hated � death
//hose � shoe
//These are not anagrams of each other:
//jump � pump
//house � home
//hotter � tinker
//levels � vessel

#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

// sorts a string from lowest ASCII value to greatest ASCII value
// uses insertion sort
// expects the string to be < 254 characters long.
void sort_str(char* str)
{
    char store_char;
    int len = strlen(str);
    for(char working_ndx = 1; working_ndx < len; ++working_ndx)
    {
        // move the char back until it is in order
        for(char insrt_ndx = working_ndx - 1; insrt_ndx >= 0; --insrt_ndx)
        {
            // swap chars if out of order
            if(str[insrt_ndx] > str[insrt_ndx + 1])
            {
                store_char = str[insrt_ndx];
                str[insrt_ndx] = str[insrt_ndx + 1];
                str[insrt_ndx + 1] = store_char;
            }
            else
            {
                break;
            }
        }
    }
}

int main()
{
    // declare string arrays
    char repeat = 'y';
    char str1[128], str2[128];

    while(repeat == 'y')
    {
        bool anagrams = false;

        // input strings
        cout << "Enter the first word:" << endl;
        cin >> str1;

        cout << "Enter the second word:" << endl;
        cin >> str2;

        cout << endl;

        // No point in further work if the lengths don't match
        if(strlen(str1) == strlen(str2))
        {
            // sort strings
            sort_str(str1);
            sort_str(str2);

            // compare strings & set anagrams to result
            if(strcmp(str1, str2) == 0)
            {
                anagrams = true;
            }
        }

        if(anagrams == true)
        {
            cout << "The words are anagrams of each other." << endl;
        }
        else
        {
            cout << "The words are NOT anagrams of each other." << endl;
        }

        cout << "play again (y/n)" << endl;
        cin >> repeat;
        while(repeat != 'y' && repeat != 'n')
        {
            cout << "Invalid response, please enter either 'y' or 'n'." << endl;
            cin >> repeat;
        }
    }

    cout << endl << endl;
    cout << "=================================================" << endl;

    return 0;
}
